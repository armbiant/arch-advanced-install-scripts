#!/bin/bash

# Install and configure specific things for server mode
# Copyright (C) 2022 - Myghi63 (contato-myghi63@protonmail.com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e 

echo -e "4-server.sh Started\n"

packages="dhcpcd openssh"

# Install packages loop
args="--needed --noconfirm"
while [ "$installation" != "finish" ] ; do
	installation="finish"
	pacman -Sy $args $packages || {
		read -p "The installation failed. Do you want to try again? [y/N]: " choice
		[ "$choice" != "y" ] && exit 1
		installation="failed"
		args="--needed"
	}
done

# Cleanup package cache
rm -rf /var/cache/pacman/pkg/*

systemctl enable dhcpcd sshd

clear
