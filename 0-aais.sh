#!/bin/bash

# Collect configurations and install arch linux by managing other scripts.
# Copyright (C) 2022 - Myghi63 (contato-myghi63@protonmail.com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

# Detect EFI
[ -d /sys/firmware/efi ] && efi="true"

# Configure audio resampling quality, 0 = worse, 10 = good, 15 = best (but 3x more CPU usage)
audio_quality="10"

echo -e "\nTesting your internet connection..."
ping -c 1 1.1.1.1 &> /dev/null || { 
	echo -e "\nThere's no internet connection!"
	exit 1
}

systemctl stop reflector.service &> /dev/null || :

# Start asking for the needed information
clear
echo -e "Let's setup! All spaces typed (except on password) will be removed! \nIf you set anything wrong, some things may not work correctly!"

# Keyboard
while [ "$keyboard_query" != "finished" ] ; do
	keyboard_query="finished"
	echo -e "\nWhat is your KEYBOARD LAYOUT? \nDefault: br"
	read -p "Your KEYBOARD LAYOUT: " -r keyboard_layout
	keyboard_layout="$(echo $keyboard_layout | sed 's/\ //g')"
	[ -z "$keyboard_layout" ] && {
		keyboard_layout="br"
		
	}
	if [ "$keyboard_layout" == "us" ] ; then 
		keymap="$keyboard_layout"
	else
		echo -e "\nWhat is your KEYBOARD VARIANT? \nDefault: abnt2"
		read -p "Your KEYBOARD VARIANT: " keyboard_variant
		keyboard_variant="$(echo $keyboard_variant | sed 's/\ //g')"
		[ -z "$keyboard_variant" ] && {
			keyboard_variant="abnt2"
		}
		keymap="$keyboard_layout-$keyboard_variant"
	fi

	[ -z "$(ls /usr/share/kbd/keymaps/**/* | grep $keymap)" ] && {
		echo -e "\nPlease insert a valid keyboard configuration!"
		keyboard_query="failed"
	}
done

# Language
while [ "$language_query" != "finished" ] ; do
	language_query="finished"
	echo -e "\nWhat will be your LANGUAGE configuration? \nDefault: pt_BR.UTF-8"
	read -p "Your LANGUAGE: " language
	language="$(echo $language | sed 's/\ //g')"
	[ -z "$language" ] && {
		language="pt_BR.UTF-8"
	}
	[ -z "$(cat /etc/locale.gen | grep $language)" ] && {
		echo -e "\nPlease insert a valid language configuration!"
		language_query="failed"
	}
done 

# Region
while [ "$region_query" != "finished" ] ; do
	region_query="finished"
	echo -e "\nWhat is your REGION? \nDefault: America/Sao_Paulo"
	read -p "Your REGION: " -r region
	region="$(echo $region | sed 's/\ //g')"
	[ -z "$region" ] && {
		region="America/Sao_Paulo"
	}
	timedatectl set-timezone "$region" || {
		echo -e "\nPlease insert a valid region configuration!"
		region_query="failed"
	}
done

# Username
while [ "$username_query" != "finished" ] ; do
	username_query="finished"
	echo -e "\nWhat will be your USERNAME?\nDefault: user"
	read -p "Your USERNAME: " user
	
	user="$(echo $user | sed -e 's/\ //g' -e 's/\(.*\)/\L\1/')"
	[ -z "$user" ] && user="user"
	echo "$user" | grep -Eq "^[a-z_][a-z0-9_-]*[$]?$" || {
		echo -e "\nInvalid username!"
		username_query="failed"
	}
done

# Password
while [ "$password_query" != "finished" ] ; do
	echo ""
	password_query="finished"
	read -p "Please type your PASSWORD: " -s password1
	password="$password1"
	echo -e "\n"
	read -p "Please type again your PASSWORD: " -s password2

	[ "$password1" != "$password2" ] && {
		echo -e "\n\nThese passwords didn't match!"
		password_query="failed"
	}
	[ -z "$password" ] && {
		echo -e "\n\nPlease type a password!"
		password_query="failed"
	}
done

# Hostname
echo -e "\n\nWhat will be the NAME OF YOUR COMPUTER?\nDefault: archpc"
read -p "Your choice: " -r hostname
hostname="$(echo $hostname | sed 's/\ //g')"
[ -z "$hostname" ] && {
	hostname="archpc"
}

# Installation Modes
echo -e "\nInstallation modes:\nDESKTOP (default): A fully configured Desktop Environment for daily usage (XFCE/Cinnamon)\nSERVER: Minimal install with ssh and dhcpcd"
read -p "Your choice: " -r installation_mode
if [ "$installation_mode" == "server" ] ; then
	echo -e "\nServer mode selected!"
else
	echo -e "\nDesktop mode selected!"
	installation_mode="desktop"
fi

[ "$installation_mode" == "desktop" ] && {
	# Desktop Environment selection
	echo -e "\nDesktop Environment selection:\nXFCE (default): brought to you by myghi63\nCinnamon (experimental): brought to you by Inky1003. Still testing.\nWARNING: If you choose Cinnamon, keep in mind that It's still being tested on this script, and It may be better in the future."
	read -p "Your choice: " -r desktop_environment
	if [ "$desktop_environment" == "cinnamon" ] ; then
		echo -e "\nCinnamon selected!"
	else
		echo -e "\nXFCE selected!"
		desktop_environment="xfce"
	fi

	echo -e "\nDo you want to automatically log-in to your account at startup?"
	read -p "Your choice [y/N]: " -r autologin
	[ "$autologin" != "y" ] && autologin="false"
}

# Disk setup
echo -e "\nIt's time to choose the disk to be installed! \nDo you want to see which disks/partitions are avaliable?\nYou will have to press letter Q to exit the overview!"
read -p "Your choice [y/N]: " -r see_partitions
[ "$see_partitions" == "y" ] && {
	clear
	fdisk -l | less
}

echo ""
while [ "$disk_setup" != "finished" ] ; do
	disk_setup="finished"
	echo -e "Please type which will be the correct DISK to install! \nExample: /dev/sda"
	read -p "Your choice: " -r disk
	disk="$(echo $disk | sed 's/\ //g')"
	if [ -b "$disk" ] ; then
		if [[ -z "$(echo $disk | grep nvme)" && -z "$(echo $disk | grep mmcblk)" && -z "$(echo $disk | grep md)" ]] ; then
			[ -z "$(echo $disk | grep [0-9])" ] || {
				echo -e "\nPlease select a disk, not a partition!"
				disk_setup="failed"
			}
		else
			[ "$(echo $disk | grep "p")" ] && {
				echo -e "\nPlease select a disk, not a partition!"
				disk_setup="failed"
			}
		fi
	else
		echo -e "\nThis disk does not exist!"
		disk_setup="failed"
	fi
done

# Optimize disk performance prompot
echo -e "\nDo you want to optimize the installation for low performance storage?\nExample: Conventional Hard Disks, removable storage, virtual machine, etc.\nNote that this can increase CPU usage on data read/write!"
read -p "Your choice [y/N]: " -r disk_performance
[ "$disk_performance" != "y" ] && disk_performance="false"

# Manual ROOT partition
echo -e "\nDo you want to set a ROOT PARTITION (for multi-boot)?"
read -p "Your choice [y/N]: " -r root_choice
if [ "$root_choice" == "y" ] ; then
	root_setup="pending"
else
	root_setup="finished"
	root_part="false"
fi

while [ "$root_setup" != "finished" ] ; do
	root_setup="finished"
	echo -e "\nPlease type which will be the correct ROOT PARTITION to install! \nExample: /dev/sda2"
	read -p "Your choice: " -r root_part
	root_part="$(echo $root_part | sed 's/\ //g')"
	if [[ -b "$root_part" ]] ; then
		[ -z "$(echo $root_part | grep [0-9])" ] && {
			echo -e "\nPlease select a partition, not a disk!"
			root_setup="failed"
		}
		[[ -z "$(echo $root_part | grep nvme)" && -z "$(echo $root_part | grep mmcblk)" ]] || {
			[ -z "$(echo $root_part | grep p)" ] && {
				echo -e "\nPlease select a partition, not a disk!"
				root_setup="failed"
			}
		}
		[ -z "$(echo $root_part | grep $disk)" ] && {
			echo -e "\nThe supplied ROOT partition must be on the previous selected disk!"
			root_setup="failed"
		}
	else
		echo -e "\nThis partition does not exist!"
		root_setup="failed"
	fi
done

# EFI partition
if [[ "$root_choice" == "y" && "$efi" == "true" ]] ; then
	efi_setup="pending"
else
	efi_setup="finished"
	efi_part="false"
fi

while [ "$efi_setup" != "finished" ] ; do
	efi_setup="finished"
	echo -e "\nPlease type which will be the correct EFI PARTITION! \nExample: /dev/sda2"
	read -p "Your choice: " -r efi_part
	efi_part="$(echo $efi_part | sed 's/\ //g')"
	if [[ -b "$efi_part" ]] ; then
		[ -z "$(echo $efi_part | grep [0-9])" ] && {
			echo -e "\nPlease select a partition, not a disk!"
			efi_setup="failed"
		}
		[[ -z "$(echo $efi_part | grep nvme)" && -z "$(echo $efi_part | grep mmcblk)" ]] || {
			[ -z "$(echo $efi_part | grep p)" ] && {
				echo -e "\nPlease select a partition, not a disk!"
				efi_setup="failed"
			}
		}
		[ -z "$(echo $efi_part | grep $disk)" ] && {
			echo -e "\nThe supplied EFI partition must be on the previous selected disk!"
			efi_setup="failed"
		}
		[ "$efi_part" == "$root_part" ] && {
			echo -e "\nThe EFI partition must be different from the ROOT partition!"
			efi_setup="failed"
		}
	else
		echo -e "\nThis partition does not exist!"
		efi_setup="failed"
	fi
done

# Encryption
echo -e "\nDo you want to encrypt your root partition?\nThis will protect your files from unauthorized physical access to your computer,\nBut you will have to type the password on early boot!"
read -p "Your choice [y/N]: " -r encryption


[ "$encryption" != "y" ] && {
	crypto_query="finished"
	crypto_password="false"
}

echo ""
while [ "$crypto_query" != "finished" ] ; do
	crypto_query="finished"
	read -p "Please type your CRYPTO-PASSWORD: " -s password1
	crypto_password="$password1"
	echo -e "\n"
	read -p "Please type again your CRYPTO-PASSWORD: " -s password2

	[ "$password1" != "$password2" ] && {
		echo -e "\n\nThese passwords didn't match!"
		crypto_query="failed"
	}
	[ -z "$password" ] && {
		echo -e "\n\nPlease type a password!"
		crypto_query="failed"
	}
done

clear

# Define partitions if not supplied
format_disk="false"
[ "$root_part" == "false" ] && { 
	format_disk="true"
	efi_part="$disk"
	root_part="$disk"
	[[ -z "$(echo $disk | grep nvme)" && -z "$(echo $disk | grep mmcblk)" ]] || {
		efi_part="$efi_part"p
		root_part="$root_part"p
	}
	if [ "$efi" == "true" ] ; then
		efi_part="$efi_part"1
		root_part="$root_part"2
	else
		efi_part="false"
		root_part="$root_part"1
	fi
}

# Review configs before installing
echo -e "Let's review your configurations! \n"
echo -e "Keyboard: $keymap\nLanguage: $language\nRegion: $region"
echo -e "Username: $user\nHostname: $hostname\nInstallation mode: $installation_mode\nDisk to be installed: $disk"
[ "$disk_performance" == "y" ] && echo -e "System will be optimized for low performance storage"
[ "$autologin" == "y" ] && echo -e "Autologin enabled for user: $user"
[ "$format_disk" == "false" ] && echo -e "ROOT partition: $root_part"
[ "$encryption" == "y" ] && echo -e "ROOT partition encryption: Enabled"
[[ "$efi_part" != "false" && "$format_disk" == "false" ]] && echo -e "EFI partition: $efi_part"

if [ "$format_disk" == "true" ] ; then
	echo -e "\nTHE SELECTED DISK WILL BE WIPED!"
else
	echo -e "\nTHE SELECTED ROOT PARTITION WILL BE WIPED!"
fi

read -p "Is everything right? Press Enter to begin or Ctrl+C to cancel: "

# Download needed files
cd ~/
rm 1-format.sh 2-install.sh 3-config.sh 4-server.sh 5-desktop.sh aais.tar.zst &> /dev/null || :

while [ "$download_scripts" != "finished" ] ; do 
	download_scripts="finished"
	
	echo -e "\nNow Downloading the neccessary scripts!\n"
	curl -O "https://gitlab.com/myghiproj/aais/-/raw/main/{1-format,2-install,3-config}.sh" || download_scripts="failed"
	[ "$installation_mode" == "server" ] && $(curl -O "https://gitlab.com/myghiproj/aais/-/raw/main/4-server.sh" || download_scripts="failed")
	[ "$installation_mode" == "desktop" ] && $(curl -O "https://gitlab.com/myghiproj/aais/-/raw/main/{5-desktop.sh,aais.tar.zst}" || download_scripts="failed")
	chmod +x ./*.sh || :

	[ "$download_scripts" == "failed" ] && {
		echo -e "\nFailed to download neccesary scripts!\nDo you want to try again? [Y/n]: "
		read -r download_choice
		[[ "$download_choice" == "y" || -z "$download_choice" ]] || exit 1
	}
done

clear

# Start Installation
bash ./1-format.sh "$disk" "$root_part" "$efi_part" "$crypto_password" "$format_disk" "$disk_performance"
bash ./2-install.sh "$root_part" "$region"
cp 3-config.sh /mnt/
arch-chroot /mnt bash 3-config.sh "$user" "$password" "$hostname" "$installation_mode" "$disk" "$root_part" "$language" "$keymap" "$region" "$crypto_password" "$format_disk" "$disk_performance"

[ "$installation_mode" == "server" ] && {
	cp 4-server.sh /mnt/
	arch-chroot /mnt bash 4-server.sh
}

[ "$installation_mode" == "desktop" ] && {
	cp 5-desktop.sh /mnt/
	arch-chroot /mnt bash 5-desktop.sh "$user" "$keyboard_layout" "$keyboard_variant" "$audio_quality" "$autologin" "$desktop_environment"
	tar --use-compress-program=unzstd -xvf aais.tar.zst
	rm -rf /mnt/home/"$user"/.config
	mv .config /mnt/home/"$user"/.config
	chown 1000:1000 -R /mnt/home/"$user"/.config
	chmod 755 -R /mnt/home/"$user"/.config
	find /mnt/home/"$user"/.config -type f -print0 | xargs -0 chmod 0644
}

# Cleanup and finish
rm /mnt/*.sh
clear
echo -e "\nThe system is now installed and configured! \n"
[[ "$format_disk" == "false" && -d /sys/firmware/efi ]] && echo -e "\nPLEASE GO TO THE FIRMWARE (BIOS) CONFIGURATION AND SET ARCH AS FIRST BOOT OPTION!"
[ "$autologin" == "y" ] && echo -e "\nYOU WILL NEED TO LOGIN ONLY ON FIRST BOOT!"
echo ""
